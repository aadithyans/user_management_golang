package controller

import (
	"UserManagement/helper"
	"UserManagement/model"
	"fmt"
	"log"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type User struct {
	ID        uint   `json:"id"`
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Email     string `json:"email"`
}

type UserList struct {
	Data []User `json:"data"`
}

func UpdateUser(context *gin.Context) {
	_, err := helper.CurrentUser(context)
	if err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	idStr := context.Param("id")
	id, err := strconv.Atoi(idStr)
	if err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": "Invalid ID"})
		return
	}

	var input model.AuthInput
	if err := context.ShouldBindJSON(&input); err != nil {
		fmt.Println("Error binding JSON:", err.Error())

		context.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request body"})
		return
	}

	user := model.User{
		FirstName: input.FirstName,
		LastName:  input.LastName,
		Password:  input.Password,
		Email:     input.Email,
	}

	err = model.UpdateUser(id, user)
	if err != nil {
		context.JSON(http.StatusInternalServerError, gin.H{"error": "Error updating user"})
		return
	}
	context.JSON(http.StatusOK, gin.H{"message": "User updated successfully"})
}

func DeleteUser(context *gin.Context) {
	_, err := helper.CurrentUser(context)
	if err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	idStr := context.Param("id")
	id, err := strconv.Atoi(idStr)
	if err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": "Invalid ID"})
		return
	}

	log.Println(id)
	err = model.DeleteUser(id)
	if err != nil {
		context.JSON(http.StatusInternalServerError, gin.H{"error": "Error deleting user"})
		return
	}
	context.JSON(http.StatusOK, gin.H{"message": "User deleted successfully"})
}

func GetUser(context *gin.Context) {
	_, err := helper.CurrentUser(context)
	if err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	idStr := context.Param("id")
	id, _ := strconv.Atoi(idStr)
	user, err := model.GetUserById(id)

	if err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	result := User{
		ID:        user.ID,
		FirstName: user.FirstName,
		LastName:  user.LastName,
		Email:     user.Email,
	}

	context.JSON(http.StatusOK, result)
}

func GetAllUsers(context *gin.Context) {
	_, err := helper.CurrentUser(context)

	if err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	users, err := model.FindAllUsers()
	if err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	var userList []User
	for _, user := range users {
		userList = append(userList, User{
			ID:        user.ID,
			FirstName: user.FirstName,
			LastName:  user.LastName,
			Email:     user.Email,
		})
	}

	result := UserList{
		Data: userList,
	}
	context.JSON(http.StatusOK, result)
}
