package model

type AuthInput struct {
	FirstName string `json:"first_name" binding"`
	LastName  string `json:"last_name" binding"`
	Email     string `json:"email" binding:"required"`
	Password  string `json:"password" binding:"required"`
}
