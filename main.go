package main

import (
	"UserManagement/controller"
	"UserManagement/database"
	"UserManagement/middleware"
	"UserManagement/model"
	"fmt"
	"log"

	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
)

func main() {
	init_configs()
	connectToDatabase()
}

func connectToDatabase() {
	database.Connect()
	database.Database.AutoMigrate(&model.User{})
	serveApplication()
}

func serveApplication() {
	router := gin.Default()
	publicRoutes := router.Group("/auth")
	publicRoutes.POST("/register", controller.Register)
	publicRoutes.POST("/login", controller.Login)

	protectedRoutes := router.Group("/api")
	protectedRoutes.Use(middleware.JWTAuthMiddleware())
	protectedRoutes.GET("/users", controller.GetAllUsers)
	protectedRoutes.GET("/users/:id", controller.GetUser)
	protectedRoutes.PUT("/users/:id", controller.UpdateUser)
	protectedRoutes.DELETE("/users/:id", controller.DeleteUser)

	router.Run(":8000")
	fmt.Println("Server running on port 8000")
}

func init_configs() {
	err := godotenv.Load(".env")
	if err != nil {
		log.Fatal("Error loading .env file")
	}
}
